package utility;

import java.util.*;

public class Date {
    private String date_str;
	private int year;
	private String month;
	private int day;
	private int hour;
	private int minute;
	private int second;
	private String week;

	public Date() {
        date_str = "none";
		year = -1;
		month = "none";
		day = -1;
		hour = -1;
		minute = -1;
		second = -1;
		week = "none";
	}

	public Date(Date _date) {
		if (_date != null) {
            date_str = _date.date_str;
			year = _date.year;
			month = _date.month;
			day = _date.day;
			hour = _date.hour;
			minute = _date.minute;
			second = _date.second;
			week = _date.week;
		}
	}

    public Date(String fullDate){
        date_str = fullDate.trim(); // discard the space at the head and tail
        String delimiter = ": ";
        StringTokenizer token = new StringTokenizer(fullDate, delimiter); // split date
        int i;
        String[] strSeg = new String[7];
        for( i = 0 ; token.hasMoreTokens() && i<7; ++i ){
            strSeg[i] = token.nextToken();
        }
        if(i!=7) return; // wrong type
        week = strSeg[0];
        month = strSeg[1];
        day = Integer.parseInt(strSeg[2]);
        hour = Integer.parseInt(strSeg[3]);
        minute = Integer.parseInt(strSeg[4]);
        second = Integer.parseInt(strSeg[5]);
        year = Integer.parseInt(strSeg[6]);
    }
	public String toString() {
		/*String str = week + " " + month + " " + day + " " + hour + ":" + minute
				+ ":" + second + " " + year;*/
		return date_str;
	}

	public String getWeek() {
		return week;
	}

	public String getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}

	public int getHour() {
		return hour;
	}

	public int getMinute() {
		return minute;
	}

	public int getSecond() {
		return second;
	}

	public int getYear() {
		return year;
	}
}
