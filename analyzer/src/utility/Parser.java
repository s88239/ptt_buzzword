package utility;

import java.util.*;
import java.io.*;

public class Parser{
    enum Type { ALL, TITLE, CONTENT, COMMENT };
    private ArticleAttr article; // article information
    private int chinessNum; // the number of Chiness compound word
    private String path, title;
    private Date date;
    private int push, boo, arrow;
    private HashMap<String, Integer> hash; // store every word string and its times

    public Parser(){
        path = "../../data/ptt_source/25.txt";
        chinessNum = 1;
        hash = new HashMap<String, Integer>();
        push = 0;
        boo = 0;
        arrow = 0;
    }

    public Parser( String _path ){
        path = _path;
        hash = new HashMap<String, Integer>();
        chinessNum = 1;
        push = 0;
        boo = 0;
        arrow = 0;
    }

    public static void main(String[] args) {
        Parser p = new Parser();
        p.parse(1);
    }
    public void setPath( String _path ){
        path = _path;
    }
    public ArticleAttr parse(int _chinessNum){ // parse all content in the article
        chinessNum = _chinessNum;
        if( !parseProcess( Type.ALL ) ) return null; // parse all, returning false means it can't process the file
        article = new ArticleAttr( hash, date, push, boo, arrow);
        return article;
    }
    public ArticleAttr parseTitle( int _chinessNum ){ // parse only the title in the article
        chinessNum = _chinessNum;
        if( !parseProcess( Type.TITLE ) ) return null; // only parse title, returning false means it can't process the file
        return new ArticleAttr( hash, date);
    }
    public ArticleAttr parseContent( int _chinessNum ){
        chinessNum = _chinessNum;
        if( !parseProcess( Type.CONTENT ) ) return null; // only parse title and content, returning false means it can't process the file
        return new ArticleAttr( hash, date);
    }
    public ArticleAttr parseComment( int _chinessNum ){
        chinessNum = _chinessNum;
        if( !parseProcess( Type.COMMENT ) ) return null; // only parse comment, returning false means it can't process the file
        return new ArticleAttr( hash, date, push, boo, arrow);
    }
    public String getTitle(){
        return title;
    }
    public Date getDate(){
        return date;
    }
    private boolean parseProcess( Type type ){ // do parsing
        try // read an article
        {
            FileInputStream fins = new FileInputStream( path );
            InputStreamReader readStream = new InputStreamReader( fins , "UTF-8");
            BufferedReader br = new BufferedReader( readStream );
            String line;
            int lineNum = 1;
            hash.clear(); // clear the hash map
            while( (line = br.readLine())!=null ){
                //System.out.println( line );
                if( isException( line ) ) continue; // the system message or signature, not the line we want
                switch( lineNum ){
                    case 1:
                        if( !isAuthor( line ) ) return false; // wrong type of the article, skip processing the file
                        break;
                    case 2:
                        if( type!=Type.COMMENT ){
                            if( !countTitle( line ) ) return false; // wrong type of the article, skip
                        }
                        break;
                    case 3:
                        putDate( line );
                        if( type==Type.TITLE ) return true; // only parse title, stop here
                        break;
                    case 4: // garbage line
                        break;
                    default:
                        if( isComment( line ) ){
                            if( type==Type.ALL || type==Type.COMMENT ) countComment( line );
                        }
                        else{
                            if( type==Type.ALL || type==Type.CONTENT ) countWords( line );
                        }
                        break;
                }
                ++lineNum;
            }
        } catch(FileNotFoundException fe){ // can't find the file
            //System.out.println( "End" );
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    private String removeRedundantStr( String line, String redundant ){ // get the substring after redundant
        int idx = -1;
        if( (idx = line.indexOf( redundant )) != -1 ){
            return line.substring( idx + redundant.length() );
        }
        return null;
    }
    private boolean countTitle( String line ){
        String fullTitle, contentTitle;
        fullTitle = removeRedundantStr( line, "標題"); // remove the title "標題"
        //System.out.println("Full: " + fullTitle);
        if( fullTitle==null ) return false; // not the stereo type of article title
        title = fullTitle.trim(); // store title without space at the head and tail
        contentTitle = removeRedundantStr( fullTitle, "]" ); // remove the classification
        //System.out.println("Content: " + contentTitle);
        countWords( contentTitle==null ? fullTitle : contentTitle );
        return true;
    }
    private void putDate( String line ){
        String fullDate = removeRedundantStr( line, "時間"); // remove the title "時間"
        if( fullDate==null ) return; // not the stereo type of article date
        date = new Date( fullDate );
        //System.out.println( date );
    }
    private void countWords( String line ){
        char word;
        String str = "";
        int i;
        boolean letterFlag = false, httpFlag = false;
        for( i = 0 ; i < line.length() ; ++i ){
            word = line.charAt( i );
            if( httpFlag ){ // concate the url
                if( isAphabet(word) || isPunct(word) ){
                    str = str + word;
                    continue;
                }
                else{
                    insertKey( str );
                    str = "";
                    httpFlag = false;
                }
            }
            if( isAphabet(word) ){ // the character is an English letter or digit
                //System.out.println( "letter:" + word );
                str = str + word; // concatenate the string
                letterFlag = true;
            }
            else{
                if( letterFlag ){ // the previous word is a letter, and this word isn't. Get the complete word.
                    if( str.equalsIgnoreCase("http") ){
                        httpFlag = true;
                        continue;
                    }
                    insertKey( str );
                }
                if( !isPunct(word) ){ // it's not a letter, space or punctuation
                    str = Character.toString( word ); // convert char to string
                    insertKey( str );
                    concatenate( line, i, str, 1); // concatenate the string of Chinese at most "chinessNum" words
                }
                str = ""; // initialized string
                letterFlag = false; // remove flag because this word isn't a letter
                httpFlag = false;
            }
        }
        if( letterFlag ) insertKey( str ); // the end of line which does not insert yet.
    }
    private void countComment( String line ){
        int firstRead =  line.indexOf(":");
        int lastRead = line.lastIndexOf("/");
        String rawComment;
        if( lastRead==-1 || lastRead-2<=firstRead) rawComment = line.substring( firstRead+1 );
        else rawComment = line.substring( firstRead+1, lastRead-2 );
        //System.out.println("Raw: " + rawComment);
        countWords( rawComment );
    }
    private void insertKey( String str ){
        int times = 1;
        if( hash.containsKey(str) ) times = 1 + hash.get( str );
        hash.put( str, times);
        //System.out.print("Insert: " + str + ", times = ");
        //System.out.println( hash.get( str ) );
    }
    private void concatenate( String str, int idx, String word, int times ){ // concatenate continuous chinese word at most "times" words
        if( times >= chinessNum || idx+1 >= str.length() ) return;
        char nextChar = str.charAt(idx+1);
        if( !isAphabet(nextChar) && !isPunct(nextChar) ){
            String newStr = word + nextChar;
            insertKey( newStr );
            concatenate( str, idx+1, newStr, ++times);
        }
    }
    private boolean isAuthor( String line ){
        return line.indexOf("作者")!=-1;
    }
    private boolean isComment( String line ){
        if( line==null || line.length() <= 1 ) return false; // this line must not be a comment
        char ch = line.charAt(0); // first character of the line
        boolean space = (line.charAt(1) == ' '); // judge second character is a space or not
        if( space==false ) return false; // not the comment type
        if( ch=='推' ) ++push;
        else if( ch=='噓' ) ++boo;
        else if( (int)ch==8594 ) ++arrow; // ch equals '→ '
        else return false; // not a comment type
        return true;
    }
    private boolean isException( String line ){ // ommit thost lines
        return line.indexOf("※ 發信站: 批踢踢實業坊(ptt.cc)")==0
        || line.indexOf("◆ From:")==0
        || line.indexOf("※ 編輯:")==0
        || line.indexOf("※ 引述")==0
        || line.indexOf("--")==0;
    }
    public static boolean isPunct(final char c) { // judge the character is a punctuation or special symbol
        final int val = (int)c;
        //System.out.print(c+":"+val+" ");
        return Character.isSpaceChar(c)
        || val >= 0 && val <= 47
        || val >= 58 && val <= 64
        || val >= 91 && val <= 96
        || val >= 0x7B && val <= 0xA0 // Gabled
        || val >= 0x2000 && val <= 0x28FF // a variety of symbols
        || val >= 0x3000 && val <= 0x303F // CJK Symbols and Punctuation
        || val >= 0xFE30 && val <= 0xFE4F // CJK Compatibility Forms
        || val >= 0xFF00 && val <= 0xFFEF; // Halfwidth and Fullwidth Forms
    }
    public static boolean isAphabet(final char word){ // judege the character is a letter or letter
        return Character.isDigit(word) || Character.isLowerCase(word) || Character.isUpperCase(word);
    }
}
