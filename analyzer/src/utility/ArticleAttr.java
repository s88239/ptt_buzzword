package utility;

import java.util.HashMap;

public class ArticleAttr {
	/**
	 * @param wordList list of word count in the article
	 * @param push count of push
	 * @param boo count of boo
	 * @param arrow count of arrow
	 */
	private HashMap<String,Integer> wordMap;
	private Date date;
	private int push;
	private int boo;
	private int arrow;
	
	public ArticleAttr(){
		//wordList = _wordList.clone();
		wordMap = new HashMap<String, Integer>();
		push = -1;
		boo = -1;
		arrow = -1;
	}
	
	public ArticleAttr(int _p, int _b, int _a, Date _date){
		wordMap = null;
		push = _p;
		boo = _b;
		arrow = _a;
		setDate(new Date( _date ));
	}
	
	public ArticleAttr(int _p, int _b, int _a){
		wordMap = null;
		push = _p;
		boo = _b;
		arrow = _a;
	}
	
	public ArticleAttr( HashMap<String,Integer> _wordList, Date _date){
		//wordList = _wordList.clone();
		wordMap = (HashMap<String, Integer>) _wordList;//.clone();
        setDate(new Date( _date ));
		push = -1;
		boo = -1;
		arrow = -1;
	}
	
	public ArticleAttr( HashMap<String,Integer> _wordList, Date _date, int _push, int _boo, int _arrow ){
		//wordList = _wordList.clone();
		wordMap = (HashMap<String, Integer>) _wordList;
        setDate(new Date( _date ));
		push = _push;
		boo = _boo;
		arrow = _arrow;
	}
	
	/**
	 * 
	 * @return word list of this article info
	 */
	public WordItem[] getList(){
		//return wordList;
		return null;
	}
	public HashMap<String, Integer> getMap(){
		return new HashMap<String, Integer>(wordMap);
	}
	/**
	 * 
	 * @return push number of this article info
	 */
	public int getPush(){
		return push;
	}
	/**
	 * 
	 * @return boo number of this article info
	 */
	public int getBoo(){
		return boo;
	}
	/**
	 * 
	 * @return arrow number of this article info
	 */
	public int getArrow(){
		return arrow;
	}
	
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
