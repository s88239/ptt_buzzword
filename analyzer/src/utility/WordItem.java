package utility;

public class WordItem {
	String word;
	int count;

	/**
	 * @param word string of word like "CCR"
	 * @param count integer of number of word
	 */
	public WordItem(String _word, int _count) {
		word = _word;
		count = _count;
	}
	
	/**
	 * @return word name
	 */
	public String name(){
		return word;
	}
	
	public void setCount(int num){
		if( num<0 ){
			return;
		}
		else{
			count = num;
			return;
		}
	}
	
	/**
	 * @return word count
	 */
	public int count(){
		return count;
	}
	/**
	 * @param cpName compare name with this word item
	 * @return true if same name 
	 */
	public boolean sameName(String cpName){
		if( cpName.equals(word) )
			return true;
		else
			return false;
	}
	
}
