package utility;
import java.util.*;
import java.io.*;
import java.net.*;

class WordBreaker{
    public static void main( String[] args ){
        Scanner scan = new Scanner(System.in);
        while(true){
            String gg = scan.next();
            new WordBreaker().createTCP(gg);
        }
    }
    public void createTCP( String line){
        String hostname = "140.109.19.104";
        int port = 1501;
        try{
            Socket connectionSock = new Socket( hostname, port);
            BufferedReader serverInput = new BufferedReader( new InputStreamReader( connectionSock.getInputStream(), "UTF-8") );
            DataOutputStream serverOutput = new DataOutputStream( connectionSock.getOutputStream());
            System.out.println("Connect!");
            String xmlMes1 = "<?xml version=\"1.0\" ?>\n<wordsegmentation version=\"0.1\" charsetcode=\"utf-8\">\n<option showcategory=\"1\" />\n<authentication username=\"s88239\" password=\"b00902098\" />\n<text>";
            String xmlMes2 = "</text>\n</wordsegmentation>";
            serverOutput.writeBytes( xmlMes1 );
            serverOutput.write( line.getBytes("UTF-8") );
            serverOutput.writeBytes( xmlMes2 );
            System.out.println("Wait for reply");
            String serverData = serverInput.readLine();
            System.out.println("斷詞: " + serverData );

            serverOutput.close();
            serverInput.close();
            connectionSock.close();
        }
        catch( IOException e ){
            System.out.println( e.getMessage() );
        }
    }
}
