package analyzer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import utility.Parser;
import utility.ImagePane;

public class PTT_Buzzword extends JFrame{
    private MainAnalyzerAgent analyzer;
    private WordMapAgent analyzer_result;
    private int weight, algorithm;
    private ImagePane homePanel, findPanel, relatedWordPanel, setPanel;
    private JTextField articleRangeBottom, articleRangeTop, libRangeBottom, libRangeTop, showBottomDate, showTopDate;
    private JTextArea inputWord;
    public PTT_Buzzword(){
        super("PTT Buzzword");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        algorithm = 0;
        weight = 0;
        analyzer = new MainAnalyzerAgent();
        homeInit();
        findInit();
        relatedWordInit();
    }
    public static void main(String[] args){
        PTT_Buzzword pttObject = new PTT_Buzzword();
        pttObject.setVisible(true);
        pttObject.setResizable(false);
    }
    private void homeInit(){
        homePanel = new ImagePane("../data/pic/background.jpg");
        homePanel.setLayout(new GridLayout(3,1));

        JLabel title = new JLabel("PTT Buzzword", JLabel.CENTER);
        title.setFont(new Font("Arial", Font.BOLD, 48));

        JPanel rangeSearch = new JPanel();
        rangeSearch.setOpaque(false);
        rangeSearch.setLayout( new GridLayout(3,1) );
        JPanel articleRangeSearch = new JPanel();
        articleRangeSearch.setOpaque(false);
        JLabel articleMessage = new JLabel("Article Range: ", JLabel.CENTER);
        articleMessage.setFont(new Font(Font.DIALOG, Font.BOLD, 20));
        articleRangeBottom = new JTextField(10);
        articleRangeTop = new JTextField(10);
        JLabel dashLine1 = new JLabel("-", JLabel.CENTER);
        dashLine1.setFont(new Font("Arial", Font.BOLD, 20));
        articleRangeSearch.add(articleMessage);
        articleRangeSearch.add(articleRangeBottom);
        articleRangeSearch.add(dashLine1);
        articleRangeSearch.add(articleRangeTop);

        JPanel datePanel = new JPanel();
        datePanel.setOpaque(false);;
        JButton showArtDateButton = new JButton("Show Article Date");
        showArtDateButton.addActionListener( new RangeAction() );
        JPanel showDateRangePanel = new JPanel();
        showDateRangePanel.setOpaque(false);
        showDateRangePanel.setLayout(new GridLayout(3,1));
        showBottomDate = new JTextField(16);
        JLabel dateDash = new JLabel("|", JLabel.CENTER);
        showTopDate = new JTextField(16);
        showDateRangePanel.add(showBottomDate);
        showDateRangePanel.add(dateDash);
        showDateRangePanel.add(showTopDate);
        JButton showLibDateButton = new JButton("Show Library Date");
        showLibDateButton.addActionListener(new RangeAction());
        datePanel.add(showArtDateButton);
        datePanel.add(showDateRangePanel);
        datePanel.add(showLibDateButton);

        JPanel libraryRangeSearch = new JPanel();
        libraryRangeSearch.setOpaque(false);
        JLabel libMessage = new JLabel("Library Range: ", JLabel.CENTER);
        libMessage.setFont(new Font("Arial", Font.BOLD, 20));
        libRangeBottom = new JTextField(Integer.toString(1),10);
        libRangeTop = new JTextField(Integer.toString(2000),10);
        JLabel dashLine2 = new JLabel("-", JLabel.CENTER);
        dashLine2.setFont(new Font("Arial", Font.BOLD, 20));
        libraryRangeSearch.add(libMessage);
        libraryRangeSearch.add(libRangeBottom);
        libraryRangeSearch.add(dashLine2);
        libraryRangeSearch.add(libRangeTop);

        rangeSearch.add( articleRangeSearch );
        rangeSearch.add( libraryRangeSearch );
        rangeSearch.add( datePanel );

        JPanel userChoosePanel = new JPanel();
        userChoosePanel.setOpaque(false);
        userChoosePanel.setLayout(new GridLayout(3,1));
        JLabel useMessage = new JLabel("Choose an algorithm to use:", JLabel.CENTER);
        useMessage.setFont(new Font("Arial", Font.BOLD, 16));
        JPanel algorithmPanel = new JPanel();
        algorithmPanel.setOpaque(false);
        JButton basicButton = new JButton("Basic Algorithm");
        basicButton.setBackground(Color.GREEN);
        JButton TFIDFButton = new JButton("TFIDF Algorithm");
        TFIDFButton.setBackground(Color.CYAN);
        JButton KNNButton = new JButton("K-NN Algorithm");
        KNNButton.setBackground(Color.BLUE);
        KNNButton.setForeground(Color.WHITE);
        basicButton.addActionListener( new RangeAction() );
        TFIDFButton.addActionListener( new RangeAction() );
        KNNButton.addActionListener( new RangeAction() );
        algorithmPanel.add( basicButton );
        algorithmPanel.add( TFIDFButton );
        algorithmPanel.add( KNNButton );

        userChoosePanel.add(useMessage);
        userChoosePanel.add(algorithmPanel);
        homePanel.add(title);
        homePanel.add(rangeSearch);
        homePanel.add(userChoosePanel);
        add( homePanel, BorderLayout.CENTER );
    }
    private void findInit(){
        findPanel = new ImagePane("../data/pic/background.jpg");
        findPanel.setLayout( new GridLayout(3,1) );
        JPanel midPanel = new JPanel();
        midPanel.setOpaque(false);
        JPanel choosePanel = new JPanel();
        choosePanel.setOpaque(false);
        choosePanel.setLayout( new GridLayout(3, 1) );
        JPanel allButtonPanel = new JPanel();
        allButtonPanel.setOpaque(false);
        JButton allButton = new JButton("Find the buzzword of all articles in the specified article range.");
	    allButton.setPreferredSize( new Dimension(470,40) );
        allButton.setBackground(Color.LIGHT_GRAY);
        allButton.addActionListener(new SwitchAction());
        allButtonPanel.add(allButton);
        JPanel relatedButtonPanel = new JPanel();
        relatedButtonPanel.setOpaque(false);
        JButton relatedButton = new JButton("Input the word to find the related buzzword.");
        relatedButton.setPreferredSize( new Dimension(470,40) );
        relatedButton.setBackground(Color.LIGHT_GRAY);
        relatedButton.addActionListener(new SwitchAction());
        relatedButtonPanel.add(relatedButton);
        JPanel setweightButtonPanel = new JPanel();
        setweightButtonPanel.setOpaque(false);
        JButton setweightButton = new JButton("Set the weight of the search method.");
        setweightButton.setPreferredSize( new Dimension(470,40) );
        setweightButton.setBackground(Color.GRAY);
        setweightButton.setForeground(Color.WHITE);
        setweightButton.addActionListener(new SwitchAction());
        setweightButtonPanel.add(setweightButton);
        choosePanel.add(allButtonPanel);
        choosePanel.add(relatedButtonPanel);
        choosePanel.add(setweightButtonPanel);
        midPanel.add(choosePanel);

        JButton backButton = new JButton("Back to input range.");
        backButton.addActionListener(new SwitchAction() );
        backButton.setPreferredSize(new Dimension(250, 70));
        backButton.setBackground(Color.PINK);
        JPanel bottomPanel = new JPanel();
        bottomPanel.setOpaque(false);
        bottomPanel.add(backButton);
        JLabel chooseMessage = new JLabel("Please press a button to choose what to do next.", JLabel.CENTER);
        chooseMessage.setFont(new Font(Font.DIALOG_INPUT, Font.ITALIC, 25) );
        findPanel.add(chooseMessage);
        findPanel.add(midPanel);
        findPanel.add(bottomPanel);
    }
    private void relatedWordInit(){
        relatedWordPanel = new ImagePane("../data/pic/background.jpg");
        relatedWordPanel.setLayout(new GridLayout(3,1));
        JLabel relatedMessage = new JLabel("Please input a word and press the submit to find all the related words.", JLabel.CENTER);
        //relatedMessage.setFont(new Font(Font.DIALOG_INPUT, Font.ITALIC, 25) );
        JPanel inputPanel = new JPanel();
        inputPanel.setOpaque(false);
        inputPanel.setLayout(new GridLayout(2,1));
        JPanel areaPanel = new JPanel();
        areaPanel.setOpaque(false);
        inputWord = new JTextArea(5,30);
        areaPanel.add(inputWord);
        inputPanel.add(areaPanel);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setOpaque(false);
        JButton clearButton = new JButton("Clear");
        clearButton.addActionListener(new SwitchAction());
        JButton submitButton = new JButton("Submit");
        submitButton.addActionListener(new SwitchAction());
        buttonPanel.add(clearButton);
        buttonPanel.add(submitButton);

        JPanel backButtonPanel = new JPanel();
        backButtonPanel.setOpaque(false);
        JButton backButton = new JButton("Back");
        backButton.addActionListener(new SwitchAction() );
        backButton.setPreferredSize(new Dimension(150, 70));
        backButton.setBackground(Color.PINK);
        backButtonPanel.add(backButton);

        inputPanel.add(buttonPanel);
        relatedWordPanel.add(relatedMessage);
        relatedWordPanel.add(inputPanel);
        relatedWordPanel.add(backButtonPanel);
    }
    private void weightSetting(){
        setPanel = new ImagePane("../data/pic/background.jpg");
        setPanel.setLayout(new GridLayout(4,1));
        JLabel title = new JLabel("Setting", JLabel.CENTER);
        title.setFont(new Font("Arial", Font.BOLD, 28));
        
        JPanel midPanel = new JPanel();
        midPanel.setOpaque(false);
        midPanel.setLayout(new GridLayout(2,1));
        JLabel setMessage = new JLabel("Please choose an appropriate weighting method to find the buzzword.", JLabel.CENTER);
        String currentSituation;
        switch(weight){
            case 0:
                currentSituation = "General";
                break;
            case 1:
                currentSituation = "Push";
                break;
            case 2:
                currentSituation = "Boo";
                break;
            case 3:
                currentSituation = "Arrow";
                break;
            case 4:
                currentSituation = "Push + Boo + Arrow";
                break;
            default:
                currentSituation = "Unknown type";
        }
        JLabel currentWeight = new JLabel("Current weighting method: " + currentSituation, JLabel.CENTER);
        midPanel.add(setMessage);
        midPanel.add(currentWeight);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setOpaque(false);
        JButton chooseGeneral = new JButton("General");
        chooseGeneral.addActionListener(new SwitchAction());
        JButton choosePush = new JButton("Push");
        choosePush.addActionListener(new SwitchAction());
        JButton chooseBoo = new JButton("Boo");
        chooseBoo.addActionListener(new SwitchAction());
        JButton chooseArrow = new JButton("Arrow");
        chooseArrow.addActionListener(new SwitchAction());
        JButton chooseTotal = new JButton("Push + Boo + Arrow");
        chooseTotal.addActionListener(new SwitchAction());
        buttonPanel.add(chooseGeneral);
        buttonPanel.add(choosePush);
        buttonPanel.add(chooseBoo);
        buttonPanel.add(chooseArrow);
        buttonPanel.add(chooseTotal);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setOpaque(false);
        JButton backButton = new JButton("Quit to change");
        backButton.addActionListener(new SwitchAction() );
        backButton.setPreferredSize(new Dimension(250, 70));
        backButton.setBackground(Color.PINK);
        bottomPanel.add(backButton);

        setPanel.add(title);
        setPanel.add(midPanel);
        setPanel.add(buttonPanel);
        setPanel.add(bottomPanel);
    }
    public void switchPanel(JPanel rm_panel, JPanel add_panel){
        remove(rm_panel);
        add( add_panel, BorderLayout.CENTER);
        validate();
        repaint();
    }
    public class RangeAction implements ActionListener{
        static final int MAX_ARTICLE = 15000;
        public void actionPerformed(ActionEvent e){
            String ab = articleRangeBottom.getText(), at = articleRangeTop.getText(), lb = libRangeBottom.getText(), lt = libRangeTop.getText();
            if( !isDigitWord(ab) || !isDigitWord(at) ){
                JOptionPane.showMessageDialog(null, "Please use integer!", "Error!", JOptionPane.ERROR_MESSAGE );
                return;
            }
            int ab_digit = Integer.parseInt(ab), at_digit = Integer.parseInt(at), lb_digit = Integer.parseInt(lb), lt_digit = Integer.parseInt(lt);
            if( ab_digit<1 || at_digit<1 || lb_digit<1 || lt_digit<1 || ab_digit>MAX_ARTICLE || at_digit>MAX_ARTICLE || lb_digit>MAX_ARTICLE || lt_digit>MAX_ARTICLE || ab_digit>at_digit || lb_digit>lt_digit ){
                JOptionPane.showMessageDialog(null, "Range out of bound!", "Error!", JOptionPane.ERROR_MESSAGE );
                return;
            }
            String buttonMes = e.getActionCommand();
            if( buttonMes.equals("Show Article Date") || buttonMes.equals("Show Library Date") ){
                String bottomArticle = buttonMes.equals("Show Article Date") ? ab : lb;
                String topArticle = buttonMes.equals("Show Article Date") ? at : lt;
                String target_bottom_path = "../data/ptt_source/" + bottomArticle + ".txt";
                String target_top_path = "../data/ptt_source/" + topArticle + ".txt";
                Parser target_bottom = new Parser(target_bottom_path);
                Parser target_top = new Parser(target_top_path);
                target_bottom.parseTitle(1);
                target_top.parseTitle(1);
                if(target_bottom.getDate()==null) showBottomDate.setText("Article not exists!");
                else showBottomDate.setText(target_bottom.getDate().toString());
                if( target_top.getDate()==null) showTopDate.setText("Article not exists!");
                else showTopDate.setText(target_top.getDate().toString());
                return;
            }
            //analyzer = new MainAnalyzerAgent();
            analyzer.setTargetPathByRange( Integer.parseInt(ab), Integer.parseInt(at) );
            analyzer.setLibPathByRange( Integer.parseInt(lb), Integer.parseInt(lt) );
            if( e.getActionCommand().equals("Basic Algorithm") ) algorithm = 0;
            else if( e.getActionCommand().equals("TFIDF Algorithm") ) algorithm = 1;
            else if( e.getActionCommand().equals("K-NN Algorithm") ){
               if( ab_digit<lb_digit || at_digit>lt_digit){
                    JOptionPane.showMessageDialog(null, "If you want to use K-NN algorithm, Article Range must include in Library Range!", "Error!", JOptionPane.ERROR_MESSAGE );
                    return;
               }
               algorithm = 2;
               switchPanel( homePanel, new ShowResultPanel(PTT_Buzzword.this, homePanel, algorithm, Test.test(Integer.parseInt(ab), Integer.parseInt(at), Integer.parseInt(lb), Integer.parseInt(lt) ) ));
               return;
            }
            switchPanel(homePanel, findPanel);
        }
        public boolean isDigitWord(String content){
            if( content.equals("") ) return false; // empty string is not a digit
            int i;
            char ch;
            for(i=0;i<content.length();++i){
                ch = content.charAt(i);
                if( !Character.isDigit(ch) ) return false;
            }
            return true;
        }
    }
    public class SwitchAction implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String buttonMes = e.getActionCommand();
            if( buttonMes.equals("Find the buzzword of all articles in the specified article range.") ){
                switchPanel( findPanel, new ShowResultPanel(PTT_Buzzword.this, findPanel, analyzer.cloneMAA(), algorithm, weight, "") );
            }
            else if( buttonMes.equals("Input the word to find the related buzzword.") ) switchPanel(findPanel, relatedWordPanel);
            else if( buttonMes.equals("Set the weight of the search method.") ){
                weightSetting();
                switchPanel(findPanel, setPanel);
            }
            else if( buttonMes.equals("Back to input range.") ) switchPanel(findPanel, homePanel);
            else if( buttonMes.equals("Back") ) switchPanel(relatedWordPanel, findPanel);
            else if( buttonMes.equals("Submit") ){
                if( inputWord.getText().equals("") ) return; // empty string
                MainAnalyzerAgent relatedAgent = analyzer.cloneMAA();
                relatedAgent.narrowPathByString(inputWord.getText());
                switchPanel(relatedWordPanel, new ShowResultPanel(PTT_Buzzword.this, relatedWordPanel, relatedAgent, algorithm, weight, inputWord.getText()));
            }
            else if( buttonMes.equals("Clear") ) inputWord.setText(""); // clean textArea
            else if( buttonMes.equals("Quit to change") || buttonMes.equals("General") || buttonMes.equals("Push") || buttonMes.equals("Boo") || buttonMes.equals("Arrow") || buttonMes.equals("Push + Boo + Arrow") ){
                if( buttonMes.equals("General") ) weight = 0;
                else if( buttonMes.equals("Push") ) weight = 1;
                else if( buttonMes.equals("Boo") ) weight = 2;
                else if( buttonMes.equals("Arrow") ) weight = 3;
                else if( buttonMes.equals("Push + Boo + Arrow") ) weight = 4;
                switchPanel(setPanel, findPanel);
            }
        }
    }
}
