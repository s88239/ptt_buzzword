package analyzer;

import utility.Parser;
import java.util.HashMap;
import java.util.Scanner;
import java.io.*;

class Test
{
	static WMAC commonWord = null, targetArticle = null;
	static HashMap<String, WordVector> feedback = new HashMap<String, WordVector>();
	public static String[] test(int tar_s, int tar_e, int all_s, int all_e) {
		feedback.clear();
		commonWord = new WMAC();
		int word_len = 4;
		int lim = all_e-all_s+1;
		String [] pathSet = new String[lim];
		for(int i=all_s-1;i<all_e;i++){
			pathSet[i-all_s+1] = "../data/ptt_source/"+(i+1)+".txt";
			//System.out.println("Path "+i+":"+pathSet[i]);
		}
		
		int start = tar_s-1, end = tar_e;
		String [] targetPath = new String[end-start];
		for(int i=0;i<end-start;i++){
			targetPath[i] = "../data/ptt_source/"+(start+i+1)+".txt";
			//System.out.println("Path "+i+":"+pathSet[i]);
		}
			
		//showArcitclesInfo( pathSet);
		
		//test, build common word table from path set, compare target article with it
		//MainAnalyzerAgnet analyzer = new MainAnalyzerAgnet();
		int wordsCount = commonWord.totalWordsCount(pathSet, word_len);
		commonWord.addArticlePath(pathSet, word_len);
		commonWord.normalize(wordsCount, lim, 100000, 1000);
		//commonWord.delLessThan((double)5);
		//commonWord.delLessThan((double)lim/(double)10);
		System.out.println("Common word table:"+commonWord.size());
		//commonWord.printSorted(500);
		targetArticle = new WMAC();
		targetArticle.addArticlePath( targetPath, word_len);
		wordsCount = targetArticle.totalWordsCount(targetPath, word_len);
		targetArticle.normalize(wordsCount, end-start, 100000, 1000);
		System.out.println("Target word table:"+targetArticle.size());

		//targetArticle.printSorted(10000);

		return targetArticle.findNearest("train.out", commonWord);
		


	}

	public static boolean setBuzz(String name, boolean isBuzz)
	{
		String xs, ys, zs, ws;
		double x, y, z, w, xx, yy, zz, ww;
		if(!targetArticle.getMap().containsKey(name))return false;
		x = targetArticle.getMap().get(name).getWordF();
		y = targetArticle.getMap().get(name).getArticleF();
		z = commonWord.getMap().get(name).getWordF();
		w = commonWord.getMap().get(name).getArticleF();
		if(feedback.containsKey(name))
		{
			xx = feedback.get(name).getWordF();
			yy = feedback.get(name).getArticleF();
			if(x == xx && y== yy)
			{
				System.out.println("The word has been set");
				return false;
			}
		}
		feedback.put(name, new WordVector(x, y, isBuzz));

		PrintWriter pw = null;
		try {
			File file = new File("train.out");
			FileWriter fw = new FileWriter(file, true);
			pw = new PrintWriter(fw);
			String buzz;
			buzz = (isBuzz)? "true": "false";
			xs = String.valueOf(x);
			ys = String.valueOf(y);
			zs = String.valueOf(z);
			ws = String.valueOf(w);
			pw.println(name+" " +buzz+" "+x+" "+y+" "+z+" "+w);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
		return true;
	}



	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("tar_s, tar_e, all_s, all_e");
		String[] ans = Test.test(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt());

		for(int i=0;i<ans.length;i++)
		{
			System.out.println(ans[i]);
		}
	}
}
