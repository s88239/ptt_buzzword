package analyzer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import utility.ArticleAttr;
import utility.Parser;

public class WordMapAgent {

	private HashMap<String, Double> map = new HashMap<String, Double>();
	private ArticleAttr info = new ArticleAttr();
	private List<String> sortedKeyArray = null;
	private int articleCount = 0;

	public WordMapAgent() {
		// map = new HashMap<String, Integer>();
	}

	public WordMapAgent(HashMap<String, Integer> _map) {
		if (_map != null) {
			// map = (HashMap<String, Integer>) _map;
			for (String key : _map.keySet()) {
				map.put(key, (double) _map.get(key));
			}
		}
	}

	public WordMapAgent(ArticleAttr _info) {
		if (_info != null) {
			// map = (HashMap<String, Integer>) info.getMap();
			// value = info.getPush();
			info = new ArticleAttr(_info.getPush(), _info.getBoo(), _info.getArrow(), _info.getDate());
			HashMap<String, Integer> _map = _info.getMap();
			for (String key : _map.keySet()) {
				map.put(key, (double) _map.get(key));
			}
		}
	}

	/**
	 * @return word number in this map
	 */
	public int size() {
		return map.size();
	}

	/**
	 * add key with num
	 * 
	 * @param key
	 *            word string
	 * @param num
	 *            word count
	 */
	private void addKN(String key, double num) {
		if (map.containsKey(key))
			num = map.get(key) + num;
		map.put(key, num);
	}

	/*
	 * private void addKNW(String key, double num){ if (map.containsKey(key))
	 * num = map.get(key) + num; map.put(key, num); }
	 */

	/**
	 * sub key with num
	 * 
	 * @param key
	 *            word string
	 * @param num
	 *            word count
	 */
	private void subKN(String key, double num) {
		if (map.containsKey(key)) {
			num = map.get(key) - num;
			map.put(key, num);
		}
	}

	/**
	 * merge two word map without weight
	 * 
	 * @param wordMapB
	 *            map to merge with
	 */
	public void mergeBasic(WordMapAgent wordMapB) {
		for (String key : wordMapB.map.keySet()) {
			this.addKN(key, wordMapB.map.get(key));
		}
	}

	/**
	 * merge mapB*weight to this map
	 * 
	 * @param wordMapB
	 * @param weight
	 */
	private void mergeWeighted(WordMapAgent wordMapB, Double weight) {
		for (String key : wordMapB.map.keySet()) {
			this.addKN(key, wordMapB.map.get(key) * weight);
		}
	}

	private void mergeIDF(WordMapAgent wordMapB) {
		for (String key : wordMapB.map.keySet()) {
			this.addKN(key, 1);
		}
	}

	/**
	 * add articles in path with word to this agent
	 * 
	 * @param pathSet
	 *            string array of path
	 * @param word
	 *            search target word
	 * @param word_len
	 *            length of Chinese word
	 * @param w_type
	 *            weight type 1-push 2-boo 3-arrow 4-total (all added target
	 *            word count) comment other-no weight
	 */
	public void addArticlePath(String[] pathSet, String word,
			int word_len, int w_type) { 
		for (int i = 0; i < pathSet.length; i++) {
			String path = pathSet[i];
			Parser pars = new Parser(path);
			WordMapAgent agenttmp = new WordMapAgent( pars.parse(word_len));
			if( agenttmp.info==null){
				System.out.println( "Null article:"+path);
				break;
			}
			
			double basic_weight = 1;
			//filter target word
			if (!agenttmp.map.containsKey(word) && word!=null) {
				continue;
			} else {
				this.articleCount ++ ;
				System.out.println(path + ":" + word + ":" + agenttmp.map.get(word) + ":" + ( agenttmp.info.getPush() + agenttmp.info.getBoo() + agenttmp.info.getArrow() )  );
			}
			//target word count add to weight
			if( word!=null ){
				basic_weight = agenttmp.map.get(word);
				System.out.println( agenttmp.info.getDate().toString() + " " +  agenttmp.map.get(word));
			}
			switch (w_type) {
			case 1: // push
				mergeWeighted(agenttmp, basic_weight
						 + agenttmp.info.getPush()
								/ (double) 100);
				break;
			case 2: // boo
				mergeWeighted(agenttmp,
						basic_weight + agenttmp.info.getBoo()
								/ (double) 100);
				break;
			case 3: // arrow
				mergeWeighted(agenttmp,
						basic_weight + agenttmp.info.getArrow()
								/ (double) 100);
				break;
			case 4: // total
				mergeWeighted(
						agenttmp,
						basic_weight
								+ (agenttmp.info.getPush()
										+ agenttmp.info.getBoo() + agenttmp.info
											.getArrow()) / (double) 300);
				break;
			default:
				mergeBasic(agenttmp);
				break;
			}

		}
	}

	/**
	 * add 1 if word appear in article at least one time
	 * @param pathSet path
	 * @param word_len 
	 */
	public void addArticlePathIDF( String[] pathSet, int word_len){
		for (int i = 0; i < pathSet.length; i++) {
			String path = pathSet[i];
			Parser pars = new Parser(path);
			WordMapAgent agenttmp = new WordMapAgent(pars.parse(word_len));
			this.mergeIDF(agenttmp);
			this.articleCount ++ ;
		}
	}
	
	
	/**
	 * count diff of two word map
	 * 
	 * @param wordMapB
	 *            diffmap = this - mapB
	 */
	public void diffBasic(WordMapAgent wordMapB) {
		for (String key : wordMapB.map.keySet()) {
			this.subKN(key, wordMapB.map.get(key));
		}
	}

	/**
	 * convert word count to TFIDF
	 * @param wordMapB word library map
	 */
	public void getTFIDF(WordMapAgent wordMapB){
		double total_word_count = 0;
		for (String key : this.map.keySet() ) {
			total_word_count = this.map.get(key);
		}
		for (String key : this.map.keySet() ) {
			if( wordMapB.map.containsKey(key) ){
				double TF = this.map.get( key) / total_word_count;
				double IDF = Math.log( wordMapB.articleCount / (1 + wordMapB.map.get( key)) );
				this.map.put( key, TF*IDF);
			}
		}
	}
	
	
	/**
	 * delete word count less than number
	 * 
	 * @param num
	 */
	public void delLessThan(double num) {
		String[] keysets = (String[]) this.map.keySet().toArray(new String[0]);
		for (String key : keysets) {
			if (map.containsKey(key)) {
				if (map.get(key) < num) {
					// map.put( key, 0);
					map.remove(key);
				}
			}
		}
	}

	/**
	 * print word hash map
	 */
	public void printBasic(int limit) {
		int i = map.size();
		for (String key : map.keySet()) {
			System.out.println(key + ":" + map.get(key));
		}
	}

	/**
	 * print word list sorted by count
	 * 
	 * @param limit
	 *            display how many, 0 as display all
	 */
	public void printSorted(int limit) {
		this.sortByCount();
		int i = sortedKeyArray.size();
		for (String name : sortedKeyArray) {
			if (limit == 0 || (i <= limit))
				System.out.println("" + i + ":" + name + ":" + map.get(name));
			i--;
		}
	}

	/**
	 * generate ranked key word array
	 */
	public void sortByCount() {
		sortedKeyArray = new ArrayList<String>(map.keySet());
		// sort list lib
		Collections.sort(sortedKeyArray, new Comparator<String>() {
			@Override
			public int compare(String sA, String sB) {
				// TODO Auto-generated method stub
				if ((map.get(sA) - map.get(sB)) > 0) {
					return 1;
				} else if ((map.get(sA) - map.get(sB)) < 0) {
					return -1;
				}
				return 0;
			}
		});
	}

	/**
	 * get i-th word name string
	 * 
	 * @param i
	 *            rank you want
	 * @return name of word
	 */
	public String getWordByRank(int i) {
		if (this.sortedKeyArray == null) {
			this.sortByCount();
		}
		if (i > this.sortedKeyArray.size() || i < 1) {
			System.out.println("Index out of bound");
			return null;
		}
		return this.sortedKeyArray.get(this.sortedKeyArray.size() - i);
	}

	/**
	 * get i-th word count double
	 * 
	 * @param i
	 *            rank you want
	 * @return count of word
	 */
	public double getCountByRank(int i) {
		if (this.sortedKeyArray == null) {
			this.sortByCount();
		}
		if (i > this.sortedKeyArray.size() || i < 1) {
			System.out.println("Index out of bound");
			return 0;
		}
		return this.map.get(this.sortedKeyArray.get(this.sortedKeyArray.size()
				- i));
	}

	public boolean hawWord( String word){
		return this.map.containsKey(word);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
