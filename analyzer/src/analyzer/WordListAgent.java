package analyzer;

import java.util.HashMap;

import utility.*;

public class WordListAgent {

	private WordItem[] wordList;
	private int value;

	/**
	 * init word list agent
	 * 
	 * @param list
	 *            input word item array
	 */
	public WordListAgent(WordItem[] list) {
		wordList = list.clone();
		//mergeInner();
	}

	public WordListAgent(ArticleAttr info) {
		wordList = info.getList().clone();
		//mergeInner();
	}
	
	/**
	 * get list info
	 * 
	 * @param lim
	 * @return first lim items in list
	 */
	public String getList(int lim) {
		String rt = "";
		for (int i = 0; i < wordList.length && i < lim; i++) {
			rt += wordList[i].name() + " " + wordList[i].count() + "\n";
		}
		return rt;
	}

	// evaluate this word list
	public int evaluate() {
		// vale =...
		return value;
	}

	/**
	 * 
	 * @param word
	 *            word to find
	 * @return index of word first occurrence in list, -1 for not found
	 */
	public int findI(String word) {
		for (int i = 0; i < this.wordList.length; i++) {
			if (wordList[i].sameName(word)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * 
	 * @param word
	 *            word to find
	 * @return index of word last occurrence in list, -1 for not found
	 */
	public int findIbkwrd(String word) {
		for (int i = this.wordList.length - 1; i >= 0; i--) {
			if (wordList[i].sameName(word)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * add word word count of index item by number
	 * 
	 * @param index
	 *            index of delete target
	 * @param number
	 *            increase count by number
	 * @return -1 for error, 0 for success
	 */
	public int addIbyN(int index, int number) {
		// bound check
		if (!(index < wordList.length) || index < 0) {
			return -1;
		}
		wordList[index].setCount( wordList[index].count() + number );
		return 0;
	}

	/**
	 * delete item from this word list
	 * 
	 * @param index
	 *            index of delete target
	 * @return -1 for error, 0 for success
	 */
	public int deleteI(int index) {
		// bound check
		if (!(index < wordList.length) || index < 0) {
			return -1;
		}
		// delete
		WordItem[] newList = new WordItem[wordList.length - 1];
		for (int i = 0; i < newList.length; i++) {
			if (i < index) {
				newList[i] = wordList[i];
			} else {
				newList[i] = wordList[i + 1];
			}
		}
		// update list
		wordList = newList;
		return 0;
	}

	/**
	 * sort word list with word count
	 */
	public void sort() {
		for (int i = 0; i < wordList.length; i++) {
			for (int j = i + 1; j < wordList.length; j++) {
				if (wordList[i].count() < wordList[j].count()) {
					WordItem tmp = wordList[i];
					wordList[i] = wordList[j];
					wordList[j] = tmp;
				}
			}
		}
	}

	/**
	 * merge repeated inner item
	 */
	public void mergeInner() {
		for (int i = 0; i < wordList.length; i++) {
			int bki = findIbkwrd(wordList[i].name());
			while (bki != i) {
				
				addIbyN(i, wordList[bki].count());
				deleteI(bki);
				bki = findIbkwrd(wordList[i].name());
			}
		}
		return;
	}

	// merge new word list to this word list
	public int mergewith(WordListAgent mrgAgent) {
		WordItem[] newList = new WordItem[ wordList.length + mrgAgent.wordList.length ];
		for(int i=0; i<newList.length; i++){
			if( i<wordList.length ){
				newList[i] = wordList[i];
			}
			else{
				newList[ i ] = mrgAgent.wordList[ i-wordList.length ];
			}
		}
		wordList = newList;
		mergeInner();
		return 0;
	}

	// word count difference of list this - input
	public WordListAgent diff(WordListAgent input) {
		return this;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**
		 * test limi item list
		 */
		int limi = 10;
		WordItem[] wordList = new WordItem[limi];
		WordItem[] wordListB = new WordItem[limi * 2];
		
		//test hash map
		HashMap<String,Integer> mapA = new HashMap<String, Integer>();
		HashMap<String,Integer> mapB = new HashMap<String, Integer>();		
		
		for (int i = 0; i < wordList.length; i++) {
			wordList[i] = new WordItem("" + i % 5, i % 4 + 1);
		}
		for (int i = 0; i < wordListB.length; i++) {
			wordListB[i] = new WordItem("" + i % 7, i % 3 + 1);
			mapB.put( ""+i%7, i%3+1);
		}
		WordListAgent testAgent = new WordListAgent(wordList);
		WordListAgent testAgentB = new WordListAgent(wordListB);
		System.out.println("List A:\n" + testAgent.getList( testAgent.wordList.length ));
		System.out.println("List B:\n" + testAgentB.getList( testAgentB.wordList.length ));
		System.out.println("Map B:\n");
		for( String key : mapB.keySet() ){
			System.out.println( key + ":" + mapB.get(key) + "");
		}
		System.out.println("Sort + merge inner...\n");
		testAgent.sort();
		testAgent.mergeInner();
		testAgentB.sort();
		testAgentB.mergeInner();
		System.out.println("List A:\n" + testAgent.getList( testAgent.wordList.length ));
		System.out.println("List B:\n" + testAgentB.getList( testAgentB.wordList.length ));
		System.out.println("Merge A+B...\n");
		testAgent.mergewith(testAgentB);
		testAgent.sort();
		System.out.println("List A:\n" + testAgent.getList( testAgent.wordList.length ));
		
		
		
	}
}
