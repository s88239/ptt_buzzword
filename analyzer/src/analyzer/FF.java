package analyzer;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Scanner;

import utility.ArticleAttr;
import utility.Parser;

public class FF {
	
	public static HashMap<String, WordVector> mapA = null, mapT = null, mapTrainA = null, mapTrainT = null;
	static HashMap<String, WordVector> feedback = new HashMap<String, WordVector>();

	public static void main(String[] args)
	{
		double x, y, z, w, min = 1000000000, t, xt, yt, zt, wt;
		String ans = "NONONO";
		String s;
		boolean buzz;
		int qq = 2;

		String file;
		Scanner scr = new Scanner(System.in);
		System.out.println("1 for all, 2 for words");
		qq = scr.nextInt();

		System.out.print("file: ");
		file = scr.next();
		mapA = new HashMap<String, WordVector>();
		mapT = new HashMap<String, WordVector>();
		mapTrainT = new HashMap<String, WordVector>();
		mapTrainA = new HashMap<String, WordVector>();

		Scanner sc = null;
		try
		{
			sc = new Scanner(new File(file));
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		while(sc.hasNext())
		{
			s = sc.next();
			if(s.equals("false"))
			{
				sc.next();
				sc.next();
				sc.next();
				sc.next();
				continue;
			}
			buzz = (sc.next().equals("true"))? true: false;
			x = Double.parseDouble(sc.next());
			y = Double.parseDouble(sc.next());
			z = Double.parseDouble(sc.next());
			w = Double.parseDouble(sc.next());
			mapT.put(s, new WordVector(x, y, buzz));
			mapA.put(s, new WordVector(z, w));
		}

		try
		{
			sc = new Scanner(new File("train.out"));
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		while(sc.hasNext())
		{
			s = sc.next();
			if(s.equals("false"))
			{
				sc.next();
				sc.next();
				sc.next();
				sc.next();
				continue;
			}
			buzz = (sc.next().equals("true"))? true: false;
			x = Double.parseDouble(sc.next());
			y = Double.parseDouble(sc.next());
			z = Double.parseDouble(sc.next());
			w = Double.parseDouble(sc.next());
			mapTrainT.put(s, new WordVector(x, y, buzz));
			mapTrainA.put(s, new WordVector(z, w));
		}

		if(qq == 1)
		{
			String[] arr = findNearest();
			for(String n: arr)
			{
				System.out.println(n);
			}
			while(true)
			{
				String fb, isBuzz;
				System.out.print("feedback: ");
				fb = scr.next();
				if(fb.equals("eof"))break;
				isBuzz = scr.next();
				if(isBuzz.equals("1") || isBuzz.equals("0"))
					setBuzz(fb, isBuzz.equals("1")? true: false);
			}
		}
		else
		{
			String kw;
			String[] arr = {"NO"};
			while(true)
			{
				System.out.print("keyword: ");
				kw = scr.next();
				if(kw.equals("eof"))break;

				arr = findNearestWord(kw);
				for(String ss: arr)
					System.out.println(ss);
			}
		}

		

	}

	public static boolean setBuzz(String name, boolean isBuzz)
	{
		String xs, ys, zs, ws;
		double x, y, z, w, xx, yy, zz, ww;
		if(!mapT.containsKey(name))return false;
		x = mapT.get(name).getWordF();
		y = mapT.get(name).getArticleF();
		z = mapA.get(name).getWordF();
		w = mapA.get(name).getArticleF();
		if(feedback.containsKey(name))
		{
			xx = feedback.get(name).getWordF();
			yy = feedback.get(name).getArticleF();
			if(x == xx && y== yy)
			{
				System.out.println("The word has been set");
				return false;
			}
		}
		feedback.put(name, new WordVector(x, y, isBuzz));

		PrintWriter pw = null;
		try {
			File file = new File("train.out");
			FileWriter fw = new FileWriter(file, true);
			pw = new PrintWriter(fw);
			String buzz;
			buzz = (isBuzz)? "true": "false";
			xs = String.valueOf(x);
			ys = String.valueOf(y);
			zs = String.valueOf(z);
			ws = String.valueOf(w);
			pw.println(name+" " +buzz+" "+x+" "+y+" "+z+" "+w);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
		return true;
	}

	public static String[] findNearest()
	{
		double x, y, z, w, t, xt, yt, zt, wt;
		double[] min = {1000000000, 1000000000, 1000000000};
		String s;
		boolean buzz;
		boolean[] arrBuzz = {false, false, false};
				ArrayList<String> result = new ArrayList<String>();

		int i = 1;
		for(String name : mapT.keySet())
		{
			if(i %1000 == 0)
			{
				System.out.println(i);
			}
			i++;
			min[0] = 100000000;
			min[1] = 100000000;
			min[2] = 100000000;
			arrBuzz[0] = false;
			arrBuzz[1] = false;
			arrBuzz[2] = false;
			x = mapT.get(name).getWordF();
			y = mapT.get(name).getArticleF();
			z = mapA.get(name).getWordF();
			w = mapA.get(name).getArticleF();
			for(String key: mapTrainT.keySet())
			{
				xt = mapTrainT.get(key).getWordF();
				yt = mapTrainT.get(key).getArticleF();
				zt = mapTrainA.get(key).getWordF();
				wt = mapTrainA.get(key).getArticleF();
				t = (x-xt)*(x-xt);
				t += (y-yt)*(y-yt);
				t += (z-zt)*(z-zt);
				t += (w-wt)*(w-wt);
				if(t < min[0])
				{
					min[2] = min[1];
					min[1] = min[0];
					min[0] = t;
					arrBuzz[2] = arrBuzz[1];
					arrBuzz[1] = arrBuzz[0];
					arrBuzz[0] = mapTrainT.get(key).getBuzz();
					continue;
				}
				if(t < min[1])
				{
					min[2] = min[1];
					min[1] = t;
					arrBuzz[2] = arrBuzz[1];
					arrBuzz[1] = mapTrainT.get(key).getBuzz();
					continue;
				}
				if(t < min[2])
				{
					min[2] = t;
					arrBuzz[2] = mapTrainT.get(key).getBuzz();
					continue;
				}
			}
			if((arrBuzz[0]||arrBuzz[1]) && (arrBuzz[1]||arrBuzz[2]) && (arrBuzz[0]||arrBuzz[2]))
			{
				result.add(name);
			}
		}
		Collections.sort(result, new Comparator<String>() {
		public int compare(String sA, String sB) {
			if( (sA.length() - sB.length()) > 0 ){
				return 1;
			}
			else if( (sA.length() - sB.length()) < 0 )
			{
				return -1;
			}
			return 0;
			}
		});
		
		int len = result.size();
		int j;
		String tmp;
		for(i=0;i<len-1;i++)
		{
			tmp = result.get(i);
			for(j=i+1;j<len;j++)
			{
				if(result.get(j).contains(tmp))break;
			}
			if(j < len)
			{
				result.remove(i);
				len--;
				i--;
			}
		}
		return result.toArray(new String[0]);
		
	}

	public static String[] findNearestWord(String aim)
	{
		double x, y, z, w, t, xt, yt, zt, wt;
		double[] min = {1000000000, 1000000000, 1000000000};
		String s;
		String[] ans = {"NO", "NO", "NO"};
		boolean buzz;
		boolean[] arrBuzz = {false, false, false};
				ArrayList<String> result = new ArrayList<String>();


			buzz = false;
			x = mapT.get(aim).getWordF();
			y = mapT.get(aim).getArticleF();
			z = mapA.get(aim).getWordF();
			w = mapA.get(aim).getArticleF();
			for(String key: mapTrainT.keySet())
			{
				xt = mapTrainT.get(key).getWordF();
				yt = mapTrainT.get(key).getArticleF();
				zt = mapTrainA.get(key).getWordF();
				wt = mapTrainA.get(key).getArticleF();
				t = (x-xt)*(x-xt);
				t += (y-yt)*(y-yt);
				t += (z-zt)*(z-zt);
				t += (w-wt)*(w-wt);
				if(t < min[0])
				{
					min[2] = min[1];
					min[1] = min[0];
					min[0] = t;
					ans[2] = ans[1];
					ans[1] = ans[0];
					ans[0] = key;
					arrBuzz[2] = arrBuzz[1];
					arrBuzz[1] = arrBuzz[0];
					arrBuzz[0] = mapTrainT.get(key).getBuzz();
					continue;
				}
				if(t < min[1])
				{
					min[2] = min[1];
					min[1] = t;
					ans[2] = ans[1];
					ans[1] = key;
					arrBuzz[2] = arrBuzz[1];
					arrBuzz[1] = mapTrainT.get(key).getBuzz();
					continue;
				}
				if(t < min[2])
				{
					min[2] = t;
					ans[2] = key;
					arrBuzz[2] = mapTrainT.get(key).getBuzz();
					continue;
				}
			}
			return ans;
		}

}
