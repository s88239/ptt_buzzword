package analyzer;                                                                                                                              

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import utility.ImagePane;

public class ShowResultPanel extends ImagePane{
    public static final int RESULT_ROW = 20;
    public static final int RESULT_COLUMN = 3;
    private PTT_Buzzword mainFrame;
    private MainAnalyzerAgent analyzer;
    private WordMapAgent analyzer_result;
    private int type, algorithm;
    private String relatedWord;
    private JPanel previousPanel, childPanel, instructionPanel, resultShowingPanel, buttonPanel, buzzwordFeedBackPanel;
    private JLabel relatedWordLabel;
    private JPanel[] resultEntry;
    private JLabel[] rowNumber, buzzwordCount;
    private JButton[] buzzwordButton;
    private String[] KNN_Result;

    public ShowResultPanel(){
        super("../data/pic/result_background.jpg");
        algorithm = 0;
    }
    public ShowResultPanel(PTT_Buzzword _frame, JPanel _previousPanel, int _algorithm, String[] _result){
        super("../data/pic/result_background.jpg");
        //setBackground(Color.GRAY);
        relatedWord = "";
        mainFrame = _frame;
        previousPanel = _previousPanel;
        algorithm = _algorithm;
        KNN_Result = _result;
        showResult();
    }
    public ShowResultPanel(PTT_Buzzword _frame, JPanel _previousPanel, MainAnalyzerAgent _analyzer, int _algorithm, int _type, String _relatedWord){
        super("../data/pic/result_background.jpg");
        setBackground(Color.GRAY);
        mainFrame = _frame;
        previousPanel = _previousPanel;
        analyzer = _analyzer;
        algorithm = _algorithm;
        type = _type;
        relatedWord = _relatedWord;
        showResult();
    }
    public void showResult(){
        switch( algorithm ){
            case 0: // Basic
                analyzer_result = analyzer.algo_basic(type);
                break;
            case 1: // TFIDF
                analyzer_result = analyzer.algo_TFIDF(type);
                break;
            case 2: // K-NN
                break;
            default:
                System.out.println("Unknown Algorithm");
        }
        int i, result_size, rankNum;
        //System.out.println("size = " + result_size);
        setLayout(new BorderLayout());

        instructionPanel = new JPanel();
        instructionPanel.setOpaque(false);
        instructionPanel.setLayout(new BorderLayout());
        buttonPanel = new JPanel();
        buttonPanel.setOpaque(false);
        JButton backButton = new JButton("Back");
        backButton.setBackground(Color.PINK);
        backButton.addActionListener(new SwitchAction());
        buttonPanel.add(backButton);
        if(algorithm!=2){
            JButton showArticleButton = new JButton("Show Articles");
            showArticleButton.addActionListener(new SwitchAction());
            buttonPanel.add(showArticleButton);
        }
        instructionPanel.add(buttonPanel, BorderLayout.WEST);
        if(algorithm==2 || !relatedWord.equals("")){
            buzzwordFeedBackPanel = new JPanel();
            buzzwordFeedBackPanel.setOpaque(false);
            relatedWordLabel = new JLabel(relatedWord, JLabel.CENTER);
            relatedWordLabel.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
            JLabel buzzwordLabel = new JLabel("Buzzword?");
            JButton yesButton = new JButton("YES");
            yesButton.addActionListener(new SwitchAction());
            JButton noButton = new JButton("NO");
            noButton.addActionListener(new SwitchAction());
            buzzwordFeedBackPanel.add(buzzwordLabel);
            buzzwordFeedBackPanel.add(yesButton);
            buzzwordFeedBackPanel.add(noButton);
            instructionPanel.add(relatedWordLabel, BorderLayout.CENTER);
            instructionPanel.add(buzzwordFeedBackPanel, BorderLayout.EAST);
        }
        add(instructionPanel, BorderLayout.NORTH);

        resultShowingPanel = new JPanel();
        resultShowingPanel.setOpaque(false);
        resultShowingPanel.setLayout(new GridLayout(RESULT_ROW,RESULT_COLUMN));
        resultEntry = new JPanel[RESULT_ROW*RESULT_COLUMN];
        rowNumber = new JLabel[RESULT_ROW*RESULT_COLUMN];
        buzzwordButton = new JButton[RESULT_ROW*RESULT_COLUMN];
        buzzwordCount = new JLabel[RESULT_ROW*RESULT_COLUMN];
        result_size = (algorithm==2)?KNN_Result.length: analyzer_result.size();
        int bound = (RESULT_ROW*RESULT_COLUMN<result_size) ? RESULT_ROW*RESULT_COLUMN : result_size;
        for( i=0 ; i<RESULT_ROW*RESULT_COLUMN ; ++i){
            rankNum = (i%RESULT_COLUMN)*RESULT_ROW + i/RESULT_COLUMN+1;
            String buzzword_str;
            double buzzword_frq;
            if(rankNum<=bound){
                buzzword_str = (algorithm==2)?KNN_Result[rankNum-1]:analyzer_result.getWordByRank(rankNum);
                buzzword_frq = (algorithm==2)?0:analyzer_result.getCountByRank(rankNum);
            }
            else{
                buzzword_str="";
                buzzword_frq = 0;
            }
            resultEntry[i] = new JPanel();
            resultEntry[i].setOpaque(false);
            resultEntry[i].setLayout(new FlowLayout(FlowLayout.LEFT));
            if(rankNum<=bound){
                rowNumber[i] = new JLabel(Integer.toString(rankNum));
                if( isNumBlur(rankNum) ) rowNumber[i].setForeground(Color.WHITE);
                buzzwordButton[i] = new JButton(buzzword_str);
                buzzwordButton[i].setBackground(Color.BLACK);
                buzzwordButton[i].setForeground(Color.WHITE);
                buzzwordButton[i].addActionListener(new FindRelatedWord());
                if(algorithm!=2){
                    buzzwordCount[i] = new JLabel(Double.toString(buzzword_frq).format("%.6f", buzzword_frq));
                    if(isFreqBlur(rankNum)) buzzwordCount[i].setForeground(Color.WHITE);
                }
                resultEntry[i].add(rowNumber[i]);
                resultEntry[i].add(buzzwordButton[i]);
                if(algorithm!=2) resultEntry[i].add(buzzwordCount[i]);
            }
            resultShowingPanel.add(resultEntry[i]);
        }
        add(resultShowingPanel, BorderLayout.CENTER);
    }
    public class FindRelatedWord implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String buttonMes = e.getActionCommand();
            if(algorithm==2){
                relatedWordLabel.setText(buttonMes);
                return;
            }
            MainAnalyzerAgent childAgent = analyzer.cloneMAA();
            childAgent.narrowPathByString(buttonMes);
            childPanel = new ShowResultPanel(mainFrame, ShowResultPanel.this, childAgent, algorithm, type, e.getActionCommand());
            mainFrame.switchPanel(ShowResultPanel.this, childPanel);
        }
    }
    public static boolean isNumBlur(int i){
        return i>=8 && i<=11 || i==13 || i==40 || i>=57 && i<=59;
    }
    private boolean isFreqBlur(int i){
        return i==12 || i==13 || i>=25 && i<=28 || i==30 || i==31 || i>=36 && i<=40 || i>=56 && i<=59;
    }
    public class SwitchAction implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String buttonMes = e.getActionCommand();
            if( buttonMes.equals("Back") ){
                mainFrame.switchPanel(ShowResultPanel.this, previousPanel);
            }
            else if( buttonMes.equals("Show Articles") ){
                ShowArticlePanel showAll = new ShowArticlePanel(mainFrame, ShowResultPanel.this, analyzer, 1);
                mainFrame.switchPanel(ShowResultPanel.this, showAll);
            }
            else if( buttonMes.equals("YES") && algorithm==2){
                if(relatedWordLabel.getText().equals("")) return;
                if(Test.setBuzz(relatedWordLabel.getText(), true)) JOptionPane.showMessageDialog(null, "Thanks for your report!", "Buzzword", JOptionPane.INFORMATION_MESSAGE );
                else JOptionPane.showMessageDialog(null, "Already Input!", "Error!", JOptionPane.ERROR_MESSAGE );
            }
            else if( buttonMes.equals("NO") && algorithm==2){
                if(relatedWordLabel.getText().equals("")) return;
                if(Test.setBuzz(relatedWordLabel.getText(), false)) JOptionPane.showMessageDialog(null, "Thanks for your report!", "Not Buzzword", JOptionPane.INFORMATION_MESSAGE );
                else JOptionPane.showMessageDialog(null, "Already Input!", "Error!", JOptionPane.ERROR_MESSAGE );
            }
        }
    }
}
