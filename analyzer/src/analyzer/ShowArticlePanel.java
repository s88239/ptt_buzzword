package analyzer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import utility.Parser;
import utility.ImagePane;

public class ShowArticlePanel extends ImagePane{
    public static int ENTRY_NUM = 20;
    private PTT_Buzzword frame;
    private MainAnalyzerAgent analyzer;
    private JPanel previousPanel, topPanel, resultPanel;
    private JLabel[] titleNum;
    private JButton[] titleButton;
    private JButton backButon, pageUpButton, pageDownButton;
    private int currentIdx;
    public ShowArticlePanel(){
        super("../data/pic/result_background.jpg");
    }
    public ShowArticlePanel(PTT_Buzzword _frame, JPanel _previousPanel, MainAnalyzerAgent _analyzer, int _currentIdx){
        super("../data/pic/result_background.jpg");
        frame = _frame;
        previousPanel = _previousPanel;
        analyzer = _analyzer;
        currentIdx = _currentIdx;
        setLayout(new BorderLayout());
        topPanel = new JPanel();
        topPanel.setOpaque(false);
        topPanel.setLayout(new BorderLayout());
        backButon = new JButton("Back");
        backButon.addActionListener(new SwitchAction());
        backButon.setBackground(Color.PINK);
        topPanel.add(backButon, BorderLayout.WEST);

        JPanel pageUpDownPanel = new JPanel();
        pageUpDownPanel.setOpaque(false);
        if(currentIdx!=1){
            pageUpButton = new JButton("Page Up");
            pageUpButton.addActionListener(new SwitchAction());
            pageUpDownPanel.add(pageUpButton);
        }
        pageDownButton = new JButton("Page Down");
        pageDownButton.addActionListener(new SwitchAction());
        pageUpDownPanel.add(pageDownButton);
        topPanel.add(pageUpDownPanel, BorderLayout.EAST);
        add(topPanel, BorderLayout.NORTH);

        resultPanel = new JPanel();
        resultPanel.setOpaque(false);
        resultPanel.setLayout(new GridLayout(ENTRY_NUM,1));
        int i, size = analyzer.getTargetPathSize(), bound;
        String article_title_str;
        //System.out.println("Size = " + size);
        bound = (size - currentIdx + 1 < ENTRY_NUM) ? size - currentIdx + 1 : ENTRY_NUM;
        JPanel[] entryPanel = new JPanel[bound];
        titleButton = new JButton[bound];
        titleNum = new JLabel[bound];
        for(i=0;i<bound;++i){
            Parser article = new Parser(analyzer.getTargetPathByIndex(currentIdx+i));
            if( article.parseTitle(1)==null || article.getTitle()==null ) article_title_str="Re:";
            else article_title_str = article.getTitle();
            entryPanel[i] = new JPanel();
            entryPanel[i].setOpaque(false);
            entryPanel[i].setLayout(new FlowLayout(FlowLayout.LEFT));
            titleNum[i] = new JLabel(Integer.toString(currentIdx+i));
            if(ShowResultPanel.isNumBlur(i+1)) titleNum[i].setForeground(Color.WHITE);
            titleButton[i] = new JButton(article_title_str);
            titleButton[i].setBackground(Color.LIGHT_GRAY);
            //titleButton[i].setForeground(Color.WHITE);
            titleButton[i].addActionListener(new SwitchAction());
            titleButton[i].setActionCommand(analyzer.getTargetPathByIndex(currentIdx+i));
            entryPanel[i].add(titleNum[i]);
            entryPanel[i].add(titleButton[i]);
            resultPanel.add(entryPanel[i]);
        }
        add(resultPanel, BorderLayout.CENTER);
    }
    public class SwitchAction implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String buttonMes = e.getActionCommand();
            if( buttonMes.equals("Back") ){
                frame.switchPanel(ShowArticlePanel.this, previousPanel);
            }
            else if( buttonMes.equals("Page Down") ){
                if(currentIdx+ENTRY_NUM>analyzer.getTargetPathSize()) return;
                frame.switchPanel(ShowArticlePanel.this, new ShowArticlePanel(frame, previousPanel, analyzer, currentIdx+ENTRY_NUM));
            }
            else if( buttonMes.equals("Page Up") ){
                if(currentIdx-ENTRY_NUM<0) return;
                frame.switchPanel(ShowArticlePanel.this, new ShowArticlePanel(frame, previousPanel, analyzer, currentIdx-ENTRY_NUM));
            }
            else{
                frame.switchPanel(ShowArticlePanel.this, new OpenArticle(frame, ShowArticlePanel.this, buttonMes) );
            }
        }
    }
}
