package analyzer;

import utility.Parser;
import java.util.Scanner;

class Train
{
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//build path list

		Scanner sc = new Scanner(System.in);

		WMAC commonWord = new WMAC();
		int word_len = 4;
		int lim = 10000;
		String [] pathSet = new String[lim];
		for(int i=0;i<lim;i++){
			pathSet[i] = "../data/ptt_source/"+(i+1)+".txt";
			//System.out.println("Path "+i+":"+pathSet[i]);
		}
		
		int start = 9000, end = 10000;
		String [] targetPath = new String[end-start];
		for(int i=0;i<end-start;i++){
			targetPath[i] = "../data/ptt_source/"+(start+i+1)+".txt";
			//System.out.println("Path "+i+":"+pathSet[i]);
		}
			
		//showArcitclesInfo( pathSet);
		
		//test, build common word table from path set, compare target article with it
		//MainAnalyzerAgnet analyzer = new MainAnalyzerAgnet();
		int wordsCount = commonWord.totalWordsCount(pathSet, word_len);
		commonWord.addArticlePath(pathSet, word_len);
		commonWord.normalize(wordsCount, lim, 100000, 1000);
		//commonWord.delLessThan((double)5);
		//commonWord.delLessThan((double)lim/(double)10);
		System.out.println("Common word table:"+commonWord.size());
		//commonWord.printSorted(500);
		WMAC targetArticle = new WMAC();
		targetArticle.addArticlePath( targetPath, word_len);
		wordsCount = targetArticle.totalWordsCount(targetPath, word_len);
		targetArticle.normalize(wordsCount, end-start, 100000, 1000);
		System.out.println("Target word table:"+targetArticle.size());

		targetArticle.printSorted(10000);

		String buzzWord;
		System.out.println("key in Buzzwords:(eof to end)");
		while(true)
		{
			buzzWord = sc.next();
			if(buzzWord.equals("eof"))break;
			targetArticle.setBuzz(buzzWord);
		}
		targetArticle.train(commonWord);


		/*WMAC test = new WMAC();
		targetPath = new String[1000];
		for(int i=0;i<1000;i++){
			targetPath[i] = "../data/ptt_source/"+(i+1)+".txt";
			//System.out.println("Path "+i+":"+pathSet[i]);
		}
		test.addArticlePath( targetPath, word_len);
		wordsCount = test.totalWordsCount(targetPath, word_len);
		test.normalize(wordsCount, 1000, 100000, 1000);
		System.out.println("test word table:"+test.size());
		System.out.println(test.findNearest("口罩", targetArticle, commonWord));*/

	}
}
