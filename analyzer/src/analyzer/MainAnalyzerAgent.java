package analyzer;

import utility.Parser;

public class MainAnalyzerAgent {
	
	private static int word_len = 4;
	private WordMapAgent commonWord;
	String [] libPathSet;
	String [] targetPathSet;
	String targetWord = null;
	
	/**
	 * self define clone function
	 * @return new MainAnalyzerAgent object
	 */
	public MainAnalyzerAgent cloneMAA(){
		MainAnalyzerAgent rt = new MainAnalyzerAgent();
		rt.commonWord = this.commonWord;
		rt.libPathSet = this.libPathSet.clone();
		rt.targetPathSet = this.targetPathSet.clone();
		return rt;
	}
	
	/**
	 * demo function : parse all article in path array and print their merged word list 
	 * @param pathSet
	 * @return
	 */
	public int showArcitclesInfo(String[] pathSet){
		WordMapAgent total = new WordMapAgent();
		for (int i = 0; i < pathSet.length; i++) {
			String path = pathSet[i];
			Parser pars = new Parser(path);
			System.out.println("Article:"+i+":"+path);
			WordMapAgent agenttmp = new WordMapAgent( pars.parse(word_len) );			
			//pars.parse();
			agenttmp.printBasic(0);
			total.mergeBasic(agenttmp);
		}
		
		System.out.println("Total:");
		//total.printBasic();
		total.printSorted(0);
		return 0;
	}
	
	/**
	 * get target number article path
	 * @param target
	 * 			number of article
	 * @return
	 * 			string of path
	 */
	private String getPath(int target){
		String pathSet = "../data/ptt_source/"+(target)+".txt";
		return pathSet;
	}
	
	/**
	 * get path set in number range
	 * @param start
	 * @param end
	 * @return
	 */
	public String[] getPathSetByRange( int start, int end){
		if( start>end ){
			System.out.println("Bound error");
			return null;
		}
		
		int i;
		String[] pathset = new String[end-start+1];
		for( i=0;i<=end-start;i++){
			pathset[i] = this.getPath(start+i); 
			//System.out.println("Path "+i+":"+pathSet[i]);
		}
		return pathset;
	}
	
	/**
	 * set word library article range
	 * @param start
	 * @param end
	 */
	public void setLibPathByRange( int start, int end){
		this.libPathSet = this.getPathSetByRange(start, end);
	}
	
	/**
	 * set search target article range
	 * @param start
	 * @param end
	 */
	public void setTargetPathByRange( int start, int end){
		this.targetPathSet = this.getPathSetByRange(start, end);
	}
	
	/**
	 * keep target article with word, delete others from path set
	 * @param word
	 */
	public void narrowPathByString( String word){
		targetWord = word;
		String[] newSet;
		int delcount = 0;
		for( int i=0; i<targetPathSet.length-delcount; ){
			String path = targetPathSet[i];
			Parser pars = new Parser(path);
			WordMapAgent agenttmp = new WordMapAgent(pars.parse(word_len));
			//filter target word
			if (!agenttmp.hawWord(word) && word!=null) {
				delcount ++;
				for( int j=i; j<targetPathSet.length-delcount; j++ ){
					targetPathSet[j] = targetPathSet[j+1];
				}
			}
			else{
				i++;
			}
		}
		newSet = new String[targetPathSet.length-delcount];
		for( int i=0; i<newSet.length; i++){
			newSet[i] = targetPathSet[i];
		}
		targetPathSet = newSet;
	}
	
	/**
	 * get size of target article path
	 * @return
	 */
	public int getTargetPathSize(){
		return this.targetPathSet.length;
	}
	
	/**
	 * get i-th target article path
	 * @param i
	 * @return
	 */
	public String getTargetPathByIndex( int i){
		if( i<=0 || i>this.targetPathSet.length )
			return null;
		return this.targetPathSet[i-1];
	}
	
	/**
	 * get result list of TF algo
	 * 
	 * @param type
	 * @return
	 */
	public WordMapAgent algo_basic( int type){
		double dellim;
		dellim = libPathSet.length / 10;
		WordMapAgent targetArticle = new WordMapAgent();
		if( this.commonWord == null ){
			commonWord = new WordMapAgent();
			//build common word table
			commonWord.addArticlePath( libPathSet, null, word_len, 0);
			//commonWord.delLessThan(dellim);
			System.out.println("Lib");
			commonWord.printSorted(100);
		}
		targetArticle.addArticlePath( targetPathSet, targetWord, word_len, type);
		//targetArticle.diffBasic( commonWord);
		targetArticle.getTFIDF( commonWord);
		return targetArticle;
	}
	
	/**
	 * get result list of TFIDF algo
	 * 
	 * @param type
	 * @return
	 */
	public WordMapAgent algo_TFIDF( int type){
		WordMapAgent targetArticle = new WordMapAgent();
		if( this.commonWord == null ){
			commonWord = new WordMapAgent();
			commonWord.addArticlePathIDF( libPathSet, word_len);
			System.out.println("IDF table");
			commonWord.printSorted(100);
		}		
		targetArticle.addArticlePath( targetPathSet, targetWord, word_len, type);
		//compare get TFIDF ver
		targetArticle.getTFIDF( commonWord);
		return targetArticle;
	}
	
		
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//test, build common word table from path set, compare target article with it
		MainAnalyzerAgent analyzer = new MainAnalyzerAgent();  
		//build path list
		analyzer.setLibPathByRange( 1, 1000);
		analyzer.setTargetPathByRange( 4001, 5000);
		
		int version = 0;	//0 count - lib, 1 TF*IDF
		
		WordMapAgent targetArticle = new WordMapAgent();
		
		//build command word table
		if( version == 0 ){
			//word count ver lib
			targetArticle = analyzer.algo_basic(0);
		}
		else{
			//IDF lib
			targetArticle = analyzer.algo_TFIDF(0);
		}
		System.out.println("After compare");
		targetArticle.printSorted(100);
		analyzer.narrowPathByString( "玩命");
		System.out.println( "Total:"+analyzer.getTargetPathSize());
		System.out.println( "First:"+analyzer.getTargetPathByIndex( 1));
		System.out.println( "Last:"+analyzer.getTargetPathByIndex( analyzer.getTargetPathSize()));
		
		if( version == 0 ){	
			targetArticle = analyzer.algo_basic(4);
		}
		else{
			//IDF lib
			targetArticle = analyzer.algo_TFIDF(4);
		}
		System.out.println("Related word");
		targetArticle.printSorted(20);
	}

}
