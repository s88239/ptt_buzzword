package analyzer;

class WordVector
{
	private double wordF, articleF;
	private boolean isBuzz;
	public WordVector(double wordF, double articleF, boolean buzz)
	{
		this.wordF = wordF;
		this.articleF = articleF;
		isBuzz = buzz;
	}
	public WordVector(double wordF, double articleF)
	{
		this(wordF, articleF, false);
	}
	public WordVector()
	{
		this(0.0, 0.0);
	}

	public void setWordF(double wordF)
	{
		this.wordF = wordF;
	}

	public void setBuzz(boolean b)
	{
		this.isBuzz = b;
	}
	public boolean getBuzz()
	{
		return isBuzz;
	}

	public void setArticleF(double articleF)
	{
		this.articleF = articleF;
	}
	public double getWordF()
	{
		return wordF;
	}
	public double getArticleF()
	{
		return articleF;
	}
}
