package analyzer;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Scanner;

import utility.ArticleAttr;
import utility.Parser;

public class WMAC {

	private HashMap<String, WordVector> map = new HashMap<String, WordVector>();
	private ArticleAttr info = new ArticleAttr();

	private List<String> keyArray;

	public WMAC() {
		// map = new HashMap<String, Integer>();
	}

	/*public WMAC(HashMap<String, Integer> _map) {
		if (_map != null){
			//map = (HashMap<String, Integer>) _map;
			for( String key : _map.keySet() ){
				map.put(key, (double)_map.get(key));
			}
		}
	}*/

	public void addArticle(ArticleAttr _info) {
		WordVector v;
		if ( _info != null){
			HashMap<String, Integer> _map = _info.getMap();
			for( String key : _map.keySet() ){
				v = new WordVector((double)_map.get(key), 1);
				if (map.containsKey(key))
				{
					v = map.get(key);
					v.setWordF(v.getWordF() + (double)_map.get(key));
					v.setArticleF(v.getArticleF() + 1.0);
				}
				map.put(key, v);
				info = new ArticleAttr( _info.getPush(), _info.getBoo(), _info.getArrow() );
			}
		}
	}

	public void setBuzz(String key)
	{
		if(map.containsKey(key))
		{
			map.get(key).setBuzz(true);
		}
	}
	
	private int totalArticleWordsCount(ArticleAttr _info) {
		int sum = 0;
		if ( _info != null){
			HashMap<String, Integer> _map = _info.getMap();
			for( String key : _map.keySet() ){
				sum += _map.get(key);
			}
		}
		return sum;
	}

	public int size() {
		return map.size();
	}

	/**
	 * add key with num
	 * 
	 * @param key
	 *            word string
	 * @param num
	 *            word count
	 */
	/*private void addKN(String key, double num) {
		if (map.containsKey(key))
			num = map.get(key) + num;
		map.put(key, num);
	}*/
	
	
	
	
	/**
	 * add articles in path to this agent
	 * @param pathSet path string array
	 * @param word_len length limit of Chinese word
	 * @param w_type weight type 1-push 2-boo 3-arrow 4-total comment other-no weight
	 */
	public void addArticlePath(String[] pathSet, int word_len){	
		System.out.println("addArticlePath:");
		Parser pars = new Parser();
		String path;
		for (int i = 0; i < pathSet.length; i++) {
			path = pathSet[i];
			pars.setPath(path);
			addArticle( pars.parse(word_len));			
			
		}
		
	}

	public int totalWordsCount(String[] pathSet, int word_len)
	{
		System.out.println("totalWordsCount:");
		Parser pars = new Parser();
		String path;
		int sum = 0;
		for (int i = 0; i < pathSet.length; i++) {
			path = pathSet[i];
			pars.setPath(path);
			sum += totalArticleWordsCount(pars.parse(word_len));
			
		}
		return sum;
	}

	public void normalize(int wordsCount,int articlesCount, double range1, double range2)
	{
		WordVector v;
		System.out.println("Normalize:");
		String[] keySet = (String[])map.keySet().toArray(new String[0]);
		for(String key: keySet)
		{
			v = map.get(key);
			if(v.getWordF()<5)
			{
				map.remove(key);
				continue;
			}
			v.setWordF(v.getWordF()*range1/(double)wordsCount);
			v.setArticleF(v.getArticleF()*range2/(double)articlesCount);
		}
	}

	/**
	 * add articles in path with word to this agent
	 * @param pathSet string array of path
	 * @param word search target word
	 * @param word_len length of Chinese word
	 * @param w_type weight type 1-push 2-boo 3-arrow 4-total (all added target word count) comment other-no weight
	 */
	/*public void addArticlePathAndWord(String[] pathSet, String word, int word_len){	
		for (int i = 0; i < pathSet.length; i++) {
			String path = pathSet[i];
			Parser pars = new Parser(path);
			WMAC agenttmp = new WMAC( pars.parse(word_len) );			
			if( !agenttmp.map.containsKey(word) ){
				continue;
			}
			else{
				System.out.println(path);
			}
					mergeBasic(agenttmp);
			}
			
		}
		
		
	}*/


	/**
	 * print word hash map
	 */
	public void printBasic(int limit) {
		int i = map.size();
		for (String key : map.keySet()) {
			System.out.println(key + ":" + map.get(key));
		}
	}

	/**
	 * print word list sorted by count
	 */
	public void printSorted(int limit) {
		/**
		 * sort list lib
		 */
		keyArray = new ArrayList<String>(map.keySet());
		Collections.sort(keyArray, new Comparator<String>() {
			@Override
			public int compare(String sA, String sB) {
				// TODO Auto-generated method stub
				if( (map.get(sA).getWordF() - map.get(sB).getWordF()) < 0 ){
					return 1;
				}
				else if( (map.get(sA).getWordF() - map.get(sB).getWordF()) > 0 ){
					return -1;
				}
				return 0;
			}
		});
		int i = keyArray.size();
		/*for (String name : keyArray) {
			if (limit == 0 || (i <= limit))
				System.out.println("" + i + ":" + name + ":" + map.get(name).getWordF()+ " - " + map.get(name).getArticleF());
			i--;
		}*/
	}

	public HashMap<String, WordVector> getMap()
	{
		return map;
	}

	public void train(WMAC all, String filename)
	{
		PrintWriter pw = null;
		HashMap<String, WordVector> mapT, mapA;
		mapA = all.getMap();
		mapT = map;
		String x, y, z, w;
	
		try {
			File file = new File(filename);
			FileWriter fw = new FileWriter(file, false);
			pw = new PrintWriter(fw);
			String buzz;
			for (String name : keyArray) {
				buzz = (mapT.get(name).getBuzz())? "true": "false";
				x = String.valueOf(mapT.get(name).getWordF());
				y = String.valueOf(mapT.get(name).getArticleF());
				z = String.valueOf(mapA.get(name).getWordF());
				w = String.valueOf(mapA.get(name).getArticleF());
				if(mapT.get(name).getWordF() > 0.65)
					pw.println(name+" " +buzz+" "+x+" "+y+" "+z+" "+w);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
	}	
	public void train(WMAC all)
	{
		train(all, "train.out");
	}

	public String[] findNearest(String file, WMAC all)
	{
		double x, y, z, w, t, xt, yt, zt, wt;
		double[] min = {1000000000, 1000000000, 1000000000};
		String s;
		boolean buzz;
		boolean[] arrBuzz = {false, false, false};
		HashMap<String, WordVector> mapA = all.getMap();
		HashMap<String, WordVector> mapTrainT = new HashMap<String, WordVector>();
		HashMap<String, WordVector> mapTrainA = new HashMap<String, WordVector>();

		ArrayList<String> result = new ArrayList<String>();

		Scanner sc = null;
		try
		{
			sc = new Scanner(new File(file));
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		while(sc.hasNext())
		{
			s = sc.next();
			if(s.equals("false"))
			{
				sc.next();
				sc.next();
				sc.next();
				sc.next();
				continue;
			}
			buzz = (sc.next().equals("true"))? true: false;
			x = Double.parseDouble(sc.next());
			y = Double.parseDouble(sc.next());
			z = Double.parseDouble(sc.next());
			w = Double.parseDouble(sc.next());
			mapTrainT.put(s, new WordVector(x, y, buzz));
			mapTrainA.put(s, new WordVector(z, w));
		}

		int i = 1;
		for(String name : map.keySet())
		{
			if(i %1000 == 0)
			{
				System.out.println(i);
			}
			i++;
			min[0] = 100000000;
			min[1] = 100000000;
			min[2] = 100000000;
			arrBuzz[0] = false;
			arrBuzz[1] = false;
			arrBuzz[2] = false;
			x = map.get(name).getWordF();
			if(x < 0.65)continue;
			y = map.get(name).getArticleF();
			z = mapA.get(name).getWordF();
			w = mapA.get(name).getArticleF();
			for(String key: mapTrainT.keySet())
			{
				xt = mapTrainT.get(key).getWordF();
				yt = mapTrainT.get(key).getArticleF();
				zt = mapTrainA.get(key).getWordF();
				wt = mapTrainA.get(key).getArticleF();
				t = (x-xt)*(x-xt);
				t += (y-yt)*(y-yt);
				t += (z-zt)*(z-zt);
				t += (w-wt)*(w-wt);
				if(t < min[0])
				{
					min[2] = min[1];
					min[1] = min[0];
					min[0] = t;
					arrBuzz[2] = arrBuzz[1];
					arrBuzz[1] = arrBuzz[0];
					arrBuzz[0] = mapTrainT.get(key).getBuzz();
					continue;
				}
				if(t < min[1])
				{
					min[2] = min[1];
					min[1] = t;
					arrBuzz[2] = arrBuzz[1];
					arrBuzz[1] = mapTrainT.get(key).getBuzz();
					continue;
				}
				if(t < min[2])
				{
					min[2] = t;
					arrBuzz[2] = mapTrainT.get(key).getBuzz();
					continue;
				}
			}
			if((arrBuzz[0]||arrBuzz[1]) && (arrBuzz[1]||arrBuzz[2]) && (arrBuzz[0]||arrBuzz[2]))
			{
				result.add(name);
			}
		}
		Collections.sort(result, new Comparator<String>() {
		public int compare(String sA, String sB) {
			if( (sA.length() - sB.length()) > 0 ){
				return 1;
			}
			else if( (sA.length() - sB.length()) < 0 )
			{
				return -1;
			}
			return 0;
			}
		});
		
		int len = result.size();
		int j;
		String tmp;
		for(i=0;i<len-1;i++)
		{
			tmp = result.get(i);
			for(j=i+1;j<len;j++)
			{
				if(result.get(j).contains(tmp))break;
			}
			if(j < len)
			{
				result.remove(i);
				len--;
				i--;
			}
		}
		return result.toArray(new String[0]);
		
	}

	public String[] findNearestWord(String aim, WMAC all)
	{
		double x, y, z, w, t, xt, yt, zt, wt;
		double[] min = {1000000000, 1000000000, 1000000000};
		String[] ans = {"NO", "NO", "NO"};
		String s;
		boolean buzz;
		boolean[] arrBuzz = {false, false, false};
		HashMap<String, WordVector> mapA = all.getMap();
		HashMap<String, WordVector> mapTrainT = new HashMap<String, WordVector>();
		HashMap<String, WordVector> mapTrainA = new HashMap<String, WordVector>();

		ArrayList<String> result = new ArrayList<String>();

		Scanner sc = null;
		try
		{
			sc = new Scanner(new File("train.out"));
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		while(sc.hasNext())
		{
			s = sc.next();
			if(s.equals("false"))
			{
				sc.next();
				sc.next();
				sc.next();
				sc.next();
				continue;
			}
			buzz = (sc.next().equals("true"))? true: false;
			x = Double.parseDouble(sc.next());
			y = Double.parseDouble(sc.next());
			z = Double.parseDouble(sc.next());
			w = Double.parseDouble(sc.next());
			mapTrainT.put(s, new WordVector(x, y, buzz));
			mapTrainA.put(s, new WordVector(z, w));
		}

	
			x = map.get(aim).getWordF();
			y = map.get(aim).getArticleF();
			z = mapA.get(aim).getWordF();
			w = mapA.get(aim).getArticleF();
			for(String key: mapTrainT.keySet())
			{
				xt = mapTrainT.get(key).getWordF();
				yt = mapTrainT.get(key).getArticleF();
				zt = mapTrainA.get(key).getWordF();
				wt = mapTrainA.get(key).getArticleF();
				t = (x-xt)*(x-xt);
				t += (y-yt)*(y-yt);
				t += (z-zt)*(z-zt);
				t += (w-wt)*(w-wt);
				if(t < min[0])
				{
					min[2] = min[1];
					min[1] = min[0];
					min[0] = t;
					ans[2] = ans[1];
					ans[1] = ans[0];
					ans[0] = key;
					arrBuzz[2] = arrBuzz[1];
					arrBuzz[1] = arrBuzz[0];
					arrBuzz[0] = mapTrainT.get(key).getBuzz();
					continue;
				}
				if(t < min[1])
				{
					min[2] = min[1];
					min[1] = t;
					ans[2] = ans[1];
					ans[1] = key;
					arrBuzz[2] = arrBuzz[1];
					arrBuzz[1] = mapTrainT.get(key).getBuzz();
					continue;
				}
				if(t < min[2])
				{
					min[2] = t;
					ans[2] = key;
					arrBuzz[2] = mapTrainT.get(key).getBuzz();
					continue;
				}
			}
			return ans;
		}

}
