package analyzer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import utility.Parser;

public class OpenArticle extends JPanel{
    private PTT_Buzzword frame;
    private JPanel previousPanel, topPanel;
    private JLabel title;
    private JScrollPane scroll;
    private JTextArea showing;
    private JButton backButon;
    public OpenArticle(){
    }
    public OpenArticle(PTT_Buzzword _frame, JPanel _previousPanel, String _path){
        frame = _frame;
        previousPanel = _previousPanel;
        setLayout(new BorderLayout());

        topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        backButon = new JButton("Back");
        backButon.addActionListener(new SwitchAction());
        backButon.setBackground(Color.PINK);
        topPanel.add(backButon, BorderLayout.WEST);

        Parser parser = new Parser(_path);
        parser.parseTitle(1);
        title = new JLabel(parser.getTitle(), JLabel.CENTER);
        title.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
        topPanel.add(title, BorderLayout.CENTER);
        add(topPanel, BorderLayout.NORTH);
        showing = new JTextArea(35,10);
        showing.setEditable(false);
        scroll = new JScrollPane( showing , ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setPreferredSize(new Dimension(100, 520));
        add(scroll, BorderLayout.SOUTH);
        try // read an article
        {
            FileInputStream fins = new FileInputStream( _path );
            InputStreamReader readStream = new InputStreamReader( fins , "UTF-8");
            BufferedReader br = new BufferedReader( readStream );
            String line;
            while( (line = br.readLine())!=null ){
                showing.setText(showing.getText()+line+"\n");
            }
        } catch(FileNotFoundException fe){ // can't find the file
            //System.out.println( "End" );
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }
    public class SwitchAction implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if( e.getActionCommand().equals("Back") ){
                frame.switchPanel(OpenArticle.this, previousPanel);
            }
        }
    }
}
