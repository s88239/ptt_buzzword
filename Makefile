main: analyzer/src/analyzer/PTT_Buzzword.java
	cd analyzer && javac -sourcepath src -d . src/analyzer/PTT_Buzzword.java
run:
	cd analyzer && java -cp . analyzer.PTT_Buzzword
clean:
	cd analyzer/analyzer && rm *.class
	cd analyzer/utility && rm *.class
