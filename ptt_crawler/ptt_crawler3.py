#!/usr/bin/python
# -*- coding: utf-8 -*-
#coding=utf-8
#vim set fileencoding=utf-8:

import telnetlib
import getpass
import Cnencoding
import re

HOST = "ptt.cc"

tn = telnetlib.Telnet(HOST)
cn = Cnencoding.Cnencoding()

#log in
print(chr(27)+"[2J")
while(True):
	user = input("Username:")
	password = getpass.getpass()
	tn.expect(["請輸入代號".encode("big5")])
	tn.write(user.encode("ascii")+b"\r")

	tn.expect(["您的密碼".encode("big5")])
	tn.write(password.encode("ascii")+b"\r")
# if username or password is wrong
	t = tn.expect(["密碼不對".encode("big5")], 0.5)
	if t[0] != -1:
		print("Wrong username or password")
		continue
	break

if tn.expect(["刪除其他重複登入的連線嗎".encode("big5")], 0.5)[0] != -1:
	tn.write(b"Y\r")

print("log in successfully")
#log in successfully, press any key to continue;
tn.expect(["任意鍵繼續".encode("big5")], 3)
tn.write(b"\r\n")

#go to gossiping board
tn.write(b"sgossiping\r\n")
print("gossiping")

#skip welcome board
if tn.expect(["任意鍵繼續".encode("big5")], 2)[0] != -1:
	tn.write(b"\r\n")

tn.read_until("eqe toh ".encode("big5"), 1) #read the screen

f = open("0.txt", "w")
fn = 6623;
begin_n = input("from(<0 for the latest one):")
if int(begin_n) > 0:
	tn.write(begin_n.encode("ascii")+b"\r")

re_ansi = re.compile("\x1b\[[0-9;]*[mABCDHJKsu]")
re_cr = re.compile("\r")

top = 1
while(True):
	tn.write("\u001b[C".encode("ascii")) #right arrow key
	tn.read_until("eqe toh ".encode("big5"), 0.1)
	tn.write("\u000c".encode("ascii"))
	s = cn.decode(tn.read_until("eqe toh ".encode("big5"), 0.1))

	s = re.sub(re_ansi, "", s)
	s = re.sub(re_cr, "", s)

	last_line_n = s.find("瀏覽 第")
	page = []
	p1 = 0
	p2 = 0
	if last_line_n != -1:
		ss = s[:last_line_n]
		last_line = s[last_line_n:]
		s = ss
		pp = last_line[last_line.find("第")+2:last_line.find("頁")-1]
		page = pp.split("/")
		try:
			p1 = int(page[0])
			p2 = int(page[1])
		except:
			tn.write("\u001b[D".encode("ascii"))
			tn.write("\u001b[B".encode("ascii")) #down arrow key
			top = 1
			continue

	if top==1:
		if "本板為PTT八卦板" in s:
			break
		f.close()
		fn = fn + 1
		if fn >= 101:
			break
		f = open(str(fn)+".txt", "w", encoding="utf-8")
		if p1==p2 and p1+p2>0:
			tn.write("\u001b[D".encode("ascii"))
			tn.write("\u001b[B".encode("ascii")) #down arrow key
			top = 1
			f.write(s)
			continue
		top = 0
	elif "(b)進板畫面" in s:
		tn.write("\u001b[B".encode("ascii")) #down arrow key
		continue
	elif "寄發新信" in s:
		tn.write("\u001b[B".encode("ascii")) #down arrow key
		print("Mail List")
		break
	else:	
		lines = s.split("\n", 1)
		if len(lines) >= 2:
			s = lines[1]+"\n"
	try:
		print(s)
	except:
		print("error")
	f.write(s)
	if p1==p2-1:
		while(True):
			tn.write("\u001b[B".encode("ascii")) #down arrow key
			tn.read_until("eqe toh ".encode("big5"), 0.1)
			tn.write("\u000c".encode("ascii"))
			s = cn.decode(tn.read_until("eqe toh ".encode("big5"), 0.1))

			s = re.sub(re_ansi, "", s)
			s = re.sub(re_cr, "", s)

			last_line_n = s.find("瀏覽 第")
			percent = 100
			if last_line_n != -1:
				ss = s[:last_line_n]
				last_line = s[last_line_n:]
				s = ss
				per_s = last_line[last_line.find("(")+1:last_line.find("%")]
				percent = int(per_s.strip())
			lines = s.rsplit("\n", 2)
			if len(lines) >= 3:
				s = lines[1]+"\n"
			try:
				print(s)
			except:
				print("error")
			f.write(s)

			if percent == 100:
				top = 1
				tn.write("\u001b[D".encode("ascii"))
				tn.write("\u001b[B".encode("ascii")) #down arrow key
				break



